package ma.s2m.itsp.project.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.s2m.itsp.project.domain.Personne;

@Repository
@Transactional
public interface PersonneRepository extends JpaRepository<Personne,Long>{

	Personne findByName(String name);
}
