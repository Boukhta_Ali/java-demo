package ma.s2m.itsp.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


public enum PersonneV2DTO {
	;
	
	public enum Request{
		;
		
		@Getter @AllArgsConstructor @ToString @NoArgsConstructor @Setter
		public static class AddPerson{
			
			private String name;
			
			private int age;
		}
		
		@Getter @AllArgsConstructor @ToString @NoArgsConstructor @Setter
		public static class UpdatePerson{
			
			private Long id;
			
			private String name;
			
			private int age;
		}
	}
	
	public enum Repsonse{
		;
		
		@Getter @AllArgsConstructor @ToString @NoArgsConstructor @Setter
		public static class AllParams{
			
			private Long id;
			
			private String name;
			
			private int age;
		}
		
		
		@Getter @AllArgsConstructor @ToString @NoArgsConstructor @Setter
		public static class LazyParams{
			
			private String name;
			
			private int age;
		}
	}
}
