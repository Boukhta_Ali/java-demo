package ma.s2m.itsp.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAmineApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAmineApplication.class, args);
	}
}
