package ma.s2m.itsp.project.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ma.s2m.itsp.project.domain.Personne;
import ma.s2m.itsp.project.dto.PersonneDTO;
import ma.s2m.itsp.project.dto.PersonneV2DTO;


@Mapper
public interface IPersonneMapper {

	IPersonneMapper instance = Mappers.getMapper(IPersonneMapper.class);
	
	static IPersonneMapper getInstance() {
		return instance;
	}

	PersonneDTO toPersonneDTO(Personne personne);
	
	PersonneV2DTO.Repsonse.LazyParams toPersonneDTOLazy(Personne personne);
	
	PersonneV2DTO.Repsonse.AllParams toPersonneDTOAll(Personne personne);
}
