package ma.s2m.itsp.project.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ma.s2m.itsp.project.domain.Personne;
import ma.s2m.itsp.project.dto.PersonneDTO;
import ma.s2m.itsp.project.dto.PersonneV2DTO;
import ma.s2m.itsp.project.mappers.IPersonneMapper;
import ma.s2m.itsp.project.services.IPersonneServcie;

@RestController
@RequestMapping("/api/personnes")
public class PersonController{

	@Autowired
	private IPersonneServcie personneService;
	
	@GetMapping
	public List<Personne> getAllPersons(){
		return personneService.getAll();
	}

	@PostMapping
	public void addPerson(@RequestBody Personne personne) {
		personneService.insertPerson(personne);
	}	
	
	@GetMapping("/{id}")
	public PersonneV2DTO.Repsonse.LazyParams getOnePersonById(@PathVariable Long id) {
		Personne personne = personneService.getOnebyId(id);
		return IPersonneMapper.getInstance().toPersonneDTOLazy(personne);
	}
	
	@GetMapping("/Byname/{name}")
	public Personne getOnePersonByName(@PathVariable String name) {
		return personneService.getOnebyName(name);
	}
	
	@PutMapping
	public void updatePerson(@RequestBody Personne personne) {
		personneService.updatePerson(personne);
	}
	
}
