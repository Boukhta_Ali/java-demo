package ma.s2m.itsp.project.services;

import java.util.List;
import org.springframework.stereotype.Service;
import ma.s2m.itsp.project.domain.Personne;

@Service
public interface IPersonneServcie {

	List<Personne> getAll();
	
	void insertPerson(Personne personne);
	
	Personne getOnebyId(Long id);
	
	Personne getOnebyName(String name);
	
	void updatePerson(Personne personne);
}
