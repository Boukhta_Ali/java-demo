package ma.s2m.itsp.project.services.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ma.s2m.itsp.project.domain.Personne;
import ma.s2m.itsp.project.repository.PersonneRepository;
import ma.s2m.itsp.project.services.IPersonneServcie;

@Service
@Transactional
public class PersonneService implements IPersonneServcie{

	@Autowired
	private PersonneRepository personneRepository;
	
	@Override
	public List<Personne> getAll() {
		return personneRepository.findAll();
	}

	@Override
	public void insertPerson(Personne personne) {
		personneRepository.save(personne);
	}

	@Override
	public Personne getOnebyId(Long id) {
		return personneRepository.findById(id).orElse(null);
	}

	@Override
	public Personne getOnebyName(String name) {
		return personneRepository.findByName(name);
	}

	@Override
	public void updatePerson(Personne personne) {
		personneRepository.save(personne);
	}
	
}
