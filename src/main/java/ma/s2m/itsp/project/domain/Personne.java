package ma.s2m.itsp.project.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
@Entity
@Table(name="PERSONNE",uniqueConstraints = {@UniqueConstraint(columnNames = {"NAME"})})
public class Personne {

	//simple comment
	@Id @GeneratedValue
	private Long id;
	
	private String name;
	
	private int age;
}
